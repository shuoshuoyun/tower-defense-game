# 按空格键生成一个怪物
import sys

import pygame
from sprites import Monster
# one=Monster.Monster1()

WIDTH = 620
HEIGHT = 420
# 怪物移动路径的坐标点
path1 = [(50, 14), (86, 14), (135, 14), (175, 14), (180, 47), (180, 92), (215, 92), (260, 92),
         (302, 94), (305, 135), (306, 174), (344,
                                             174), (392, 174), (430, 180), (430, 139),
         (430, 90), (430, 51), (475, 52), (516, 52), (560,
                                                      52), (560, 94), (560, 131), (560, 166),
         (560, 205), (560, 247), (560, 273), (560,
                                              307), (513, 305), (469, 305), (432, 305),
         (387, 305), (349, 305), (300, 305), (250, 305), (200, 305)]


'''主函数'''
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
bg = (0, 0, 0)
background = pygame.image.load('./resource/imgs/game/map.jpg').convert()
pygame.display.set_caption("塔防-公众号: python趣味爱好者")
clock = pygame.time.Clock()
monsters = []


def monster_move():
    for i in monsters:
        i.index1 += i.speed


def create_monster():
    monsters.append(Monster.Monster1())
    # 产生怪物


def draw_monster():
    global monsters
    for j in monsters:
        if j.index1 >= len(path1):
            monsters.remove(j)
    for i in monsters:
        screen.blit(i.image, path1[i.index1])


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                create_monster()
    monster_move()
    screen.blit(background, (0, 0))
    draw_monster()
    clock.tick(4)
    pygame.display.flip()
