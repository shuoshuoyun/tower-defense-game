
import pygame
from sprites import Bullet


'''炮塔类'''


class Tower(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        #self.imgs = ['./resource/imgs/game/basic_tower.png', './resource/imgs/game/med_tower.png', './resource/imgs/game/heavy_tower.png']
        self.image = pygame.image.load('./resource/imgs/game/tower1_45-33.JPG')
        self.rect = self.image.get_rect()
        # 初始化一个子弹
        self.arrow = Bullet.bullet1()
        # 当前的位置
        self.coord = 0, 0
        self.position = 0, 0
        self.rect.left, self.rect.top = self.position
        self.reset()
    '''射击'''

    def shot(self, position, angle=None):  # 参数是子弹的位置和角度
        bullet = None
        if not self.is_cooling:
            bullet = Bullet.bullet1()  # 初始化一个子弹
            bullet.reset(position, angle)
            self.is_cooling = True  # 子弹重新进入冷却时间
        if self.is_cooling:
            self.coolTime -= 1
            if self.coolTime == 0:  # 冷却时间结束以后
                self.reset()
        return bullet
    '''重置'''

    def reset(self):
        self.price = 500
        # 射箭的冷却时间
        self.coolTime = 30
        # 是否在冷却期
        self.is_cooling = False
