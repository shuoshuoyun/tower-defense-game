
import math
import random
import pygame


'''子弹类'''


class bullet1(pygame.sprite.Sprite):
    def __init__(self):
        assert arrow_type in range(3)
        pygame.sprite.Sprite.__init__(self)
        # 载入子弹的图片
        self.image = pygame.image.load('./resource/imgs/game/bullet1.png')
        self.rect = self.image.get_rect()
        self.position = 0, 0
        self.rect.left, self.rect.top = self.position
        # 与水平向左的直线所成的夹角, 顺时针为正
        self.angle = 0  # 子弹的射击角度
        self.speed = 4  # 子弹的移动速度
        self.scope = 30  # 子弹的射击范围
        self.attack_power = 5  # 子弹的杀伤力

    '''不停移动'''

    def move(self):
        self.position = self.position[0] - self.speed * math.cos(
            self.angle), self.position[1] - self.speed * math.sin(self.angle)
        self.rect.left, self.rect.top = self.position
    '''重置子弹的位置'''

    def reset(self, position, angle=None):
        if angle is None:
            angle = random.random() * math.pi * 2
        self.position = position
        self.angle = angle
        self.rect = self.image.get_rect()
