
import pygame


'''敌方类'''
class Monster1(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)

		self.image = pygame.image.load('./resource/imgs/game/monster.png')
		
		self.rect = self.image.get_rect()
		# 走过的路(避免重复走，保证去攻击城堡)
		#self.reached_path = []
		# 在道路某个单元中移动的距离, 当cell_move_dis大于单元长度时移动到下一个到了单元并置0该变量
		#self.cell_move_dis = 0
		# 当前所在的位置
		#self.coord = 3, 2
		self.index1=0#目前所在路径列表中的位置
		self.position = 60, 40
		self.rect.left, self.rect.top = self.position
		# 最大生命值
		self.max_life_value = 20
		# 当前生命值
		self.life_value = 20
		# 速度
		self.speed = 1
		# 击杀奖励
		self.reward = 100
		# 对大本营造成的伤害
		self.damage = 1

	'''不停地移动'''
	def move(self, the_path):
		if self.index1+self.speed<len(the_path):
			self.index1=self.index1+self.speed
			return 0#意思是没有攻击到目标点
		else:
			return 1#意思是已经攻击到了目标点

