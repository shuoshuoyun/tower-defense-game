# 塔防游戏

## 介绍
python趣味爱好者塔防游戏代码

## Python版本及相关模块
`python == 3.6.4`

`pygame`

## 游戏目录结构说明
```
|-- maps            地图
|-- resource        游戏素材
|-- sprites         游戏精灵代码
|-- contry.py       运行文件
```

## 运行方法
直接运行  `control.py` 文件即可

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
